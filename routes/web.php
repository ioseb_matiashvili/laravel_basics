<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');
Route::get('/about', 'PagesController@about');
Route::get('/services', 'PagesController@services');

Route::resource('posts', 'PostsController');

// Some routes in Laravel

// Route::get('/users/{id}', function($id) {
//     return 'This user by ID = '.$id;
// });

// Route::get('/users/{name}/{id}', function($name, $id) {
//     return 'User name is '.$name.' with ID = '.$id; 
// });
Auth::routes();

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

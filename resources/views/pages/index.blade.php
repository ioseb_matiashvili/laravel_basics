@extends('layouts.app')

@section('content')
    <div class="jumbotron text-center">
        <h1>Welcome to Laravel!</h1>
        <p>Basic webiste</p>
        <p><a class='btn btn-primary btn-lg'  href="/login" role="button">Login</a>
            <a class='btn btn-primary btn-lg'  href="/register" role="button">Register</a></p>
    </div>
@endsection
<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
<h5 class="my-0 mr-md-auto font-weight-normal">{{ config('app.name', 'LSAPP') }}</h5>
    <nav class="my-2 my-md-0 mr-md-3">
      <a class="p-2 text-dark" href="/">Home</a>
      <a class="p-2 text-dark" href="/about">About</a>
      <a class="p-2 text-dark" href="/services">Services</a>
      <a class="p-2 text-dark" href="/posts">Blog</a>
      <a class="p-2 text-dark" href="/posts/create">Create Post</a>
    </nav>
      @if(Auth::guest()) 
        <a class="p-2 text-dark" href="{{ route('login') }}">Login</a>
        <a class="p-2 text-dark" href="{{ route('register') }}">Register</a>
      @else
        <nav class="nav-item dropdown">
          <a class="p2 nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}</a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="/dashboard">Dashboard</a>
            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); 
                                        document.getElementById('logout-form').submit();">Logout</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST">
              {{ csrf_field() }}
            </form>
          </div>
        </nav>
      @endif
    {{-- <ul class="nav nabar-nav navbar-right">
      <li><a href="/posts/create">Create Post</a></li>
    </ul> --}}
    <!-- <a class="btn btn-outline-primary" href="#">Sign up</a> -->
</div>